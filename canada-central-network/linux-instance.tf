resource "azurerm_network_interface" "final-project" {
  name                = "final-project-nic"
  location            = azurerm_resource_group.final-project.location
  resource_group_name = azurerm_resource_group.final-project.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.final-project.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.final-project.id
  }
}

resource "azurerm_linux_virtual_machine" "final-project" {
  name                = "final-project-machine"
  resource_group_name = azurerm_resource_group.final-project.name
  location            = azurerm_resource_group.final-project.location
  size                = "Standard_D2_v2"
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.final-project.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
}
