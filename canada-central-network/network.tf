resource "azurerm_virtual_network" "final-project" {
  name                = "final-project-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.final-project.location
  resource_group_name = azurerm_resource_group.final-project.name
}

resource "azurerm_subnet" "final-project" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.final-project.name
  virtual_network_name = azurerm_virtual_network.final-project.name
  address_prefixes     = ["10.0.0.0/24"]
}

resource "azurerm_public_ip" "final-project" {
  name                = "final-project-machine-pip"
  resource_group_name = azurerm_resource_group.final-project.name
  location            = azurerm_resource_group.final-project.location
  allocation_method   = "Static"
}

