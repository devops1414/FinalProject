terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  subscription_id = "ebbe9291-7761-4f5c-a534-47ffac859e99"
  features {}
}

# Create a resource group
resource "azurerm_resource_group" "final-project" {
  name     = "final-group-rg"
  location = "Canada Central"
}
